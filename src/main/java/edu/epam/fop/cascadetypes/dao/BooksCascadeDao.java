package edu.epam.fop.cascadetypes.dao;

import edu.epam.fop.cascadetypes.entity.Author;
import edu.epam.fop.cascadetypes.entity.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BooksCascadeDao {
    private final SessionFactory sessionFactory;

    public BooksCascadeDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void demonstratePersist() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Author author = new Author();
            author.setName("John Doe");

            Book book = new Book();
            book.setTitle("Hibernate Basics");
            book.setAuthor(author);
            author.getBooks().add(book);

            // The persist method will save the author and the book
            session.persist(author);
            session.getTransaction().commit();
        }
    }

    public void demonstrateMerge() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Author author = new Author();
            author.setName("Jane Smith");
            Book book = new Book();
            book.setTitle("Before merge");
            book.setAuthor(author);
            author.getBooks().add(book);
            session.persist(author);
            session.getTransaction().commit();

            session.beginTransaction();
            author.setName("Jane Brown");
            author.getBooks().forEach(b -> b.setTitle("After merge"));
            // The merge method will update the author and the book
            // even though they were not attached to the session
            session.merge(author);
            session.getTransaction().commit();
        }
    }

    public void demonstrateRemove() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Author author = new Author();
            author.setName("Emily Johnson");
            Book book = new Book();
            book.setTitle("To be removed");
            book.setAuthor(author);
            author.getBooks().add(book);
            session.persist(author);
            session.getTransaction().commit();

            // Removing the author will also remove the book
            session.beginTransaction();
            session.remove(author);
            session.getTransaction().commit();
        }
    }

    public void demonstrateRefresh() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Author author = new Author();
            author.setName("Michael Williams");
            Book book = new Book();
            book.setTitle("To be refreshed");
            book.setAuthor(author);
            author.getBooks().add(book);
            session.persist(author);
            session.getTransaction().commit();

            session.beginTransaction();
            author.setName("Michael Jackson");
            book.setTitle("Refreshed");
            session.refresh(author);
            session.getTransaction().commit();

            // Even though we changed the author and book objects, the refresh method
            // will reload the data from the database and overwrite the changes
            System.out.println("\nAuthor: " + author.getName());
            System.out.println("Book: " + book.getTitle());
        }
    }

    public void demonstrateDetach() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Author author = new Author();
            author.setName("Emma Brown");
            Book book = new Book();
            book.setTitle("To be detached");
            book.setAuthor(author);
            author.getBooks().add(book);
            session.persist(author);
            session.getTransaction().commit();
            // The author and book objects are attached to the session
            // output: Is author attached: true
            //         Is book attached: true
            System.out.println("\nIs author attached: " + session.contains(author));
            System.out.println("Is book attached: " + session.contains(book));

            session.beginTransaction();
            session.detach(author);
            session.getTransaction().commit();

            // The author and book objects are detached from the session
            // output: Is author attached: false
            //         Is book attached: false
            System.out.println("\nIs author attached: " + session.contains(author));
            System.out.println("Is book attached: " + session.contains(book));
        }
    }
}
