package edu.epam.fop.cascadetypes;

import edu.epam.fop.cascadetypes.dao.BooksCascadeDao;
import edu.epam.fop.cascadetypes.entity.Author;
import edu.epam.fop.cascadetypes.entity.Book;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Demo {
    public static void main(String[] args) {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml")
                .addAnnotatedClass(Author.class)
                .addAnnotatedClass(Book.class);
        try (SessionFactory sessionFactory = configuration.buildSessionFactory()) {
            BooksCascadeDao booksCascadeDao = new BooksCascadeDao(sessionFactory);
            // booksCascadeDao.demonstratePersist();
            // booksCascadeDao.demonstrateMerge();
            // booksCascadeDao.demonstrateRemove();
            // booksCascadeDao.demonstrateRefresh();
            booksCascadeDao.demonstrateDetach();
        }
    }
}
